/**
 * Created by hl on 2017/8/9.
 */
var restify = require('restify');
var builder = require('botbuilder');
var request = require('request');
var dateformat = require('dateFormat');
var hlsa = require('./hlsqlapi.js');
var parse = require('./parse');
var fs = require("fs");
var http = require("http");
var nrClient = require("node-rest-client").Client;

//資料庫存取API路徑
var hlsqlapi_host = "http://hlsqlapi.azurewebsites.net";//正式資料連接存取網址--Azure SQL
//var hlsqlapi_host = "http://localhost:3000"; //本機資料連接存取網址--測試用
var hlsqlapi_path_saveOrder = "/order/save"; //儲存訂單
var hlsqlapi_path_savetalk = "/order/savetalk"; //儲存對話

//本機檔案路徑

//服務路徑
//var hostUrl = "https://hlnodebot.azurewebsites.net";
//var hostPort = 8000;
var port = process.env.PORT || 3000;
//var hostUrl = "localhost";
//var hostPort = 3979;

// Setup Restify Server
/*var server = restify.createServer();
server.listen(hostPort,hostUrl, function () {
    console.log('%s listening to %s', server.name, server.url);
});*/

var server = restify.createServer();
/*server.listen(hostPort,hostUrl,function () {
    console.log('%s listening to %s', server.name, server.url);
});*/
server.listen(port);
// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId:"ff831480-7f2a-482f-a577-7617bfe190d1" ,
    appPassword: "GSjXDcbfNzSmJbunHE4DYA9"
});

// Listen for messages from users
server.post('/api/messages', connector.listen());

// Receive messages from the user and respond by echoing each message back (prefixed with 'You said:')
var bot = new builder.UniversalBot(connector,
    function (session) {
        if(session.message && session.message.value){
            processSubmitAction(session,session.message.value);
            return;
        }
    }
);
var LUIS_MODEL_URL = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/dfd552ec-0eba-4843-9cba-0e29a28652ed?subscription-key=4247f4e0f77444859f3a6d0632cbfef0&timezoneOffset=0&verbose=true&q=";
var recognizer = new builder.LuisRecognizer(LUIS_MODEL_URL);
bot.recognizer(recognizer);

bot.dialog("單筆購書",[
    function (session,args,next) {
        wlog("單筆購書");
        session.send("單筆購書");
        testapi2();
        saveTalk(session);
        var intent = args.intent;
        //console.log(JSON.stringify(intent));
        //wlog("Intent:"+JSON.stringify(intent));
        var queryStr = analyzeBookName(intent);//可能是書名
        var orderNum = analyzeOrderNum(intent);//可能是數量
        var orderMemo = analyzeOrderMemo(intent);//可能外註
        var orderCustom = analyzeOrderCustom(intent);//可能客戶名
        //有查詢字串
        session.send("可能是書名-->"+queryStr);
        session.send("可能是訂購數量-->"+orderNum);
        session.send("可能是外註-->"+orderMemo);
        session.send("可能是客戶-->"+orderCustom);

        if(queryStr!=""){
            //有書名查詢
            hlsa.query2(queryStr,function (qResp) {
                console.log("qResp:"+JSON.stringify(qResp));
                //額外資訊加入Adaptive Cards  JSON - data 區塊
                //加入客戶
                qResp["content"]["actions"][0].data["CUSTOM_ID"] = orderCustom;
                //加入數量
                qResp["content"]["actions"][0].data["ORDER_NUM"] = orderNum;
                //加入時間
                var nowDate = new Date();
                var dateStr = dateformat(nowDate,"yyyy-mm-dd HH:MM:ss.l");
                qResp["content"]["actions"][0].data["ORDER_TIME"] = dateStr;
                //加入備註
                qResp["content"]["actions"][0].data["MEMO"] = orderMemo;
                console.log("new qResp:"+JSON.stringify(qResp));
                var card = qResp;
                var msg = new builder.Message(session).addAttachment(card);
                session.send(msg);
            });
        }

        session.endDialog();
    }
]).triggerAction({
    matches: '單筆購書'
});

bot.dialog("多筆訂書",[
    function (session,args,next) {
        wlog("多筆訂書");
        session.send("多筆訂書");
        var intent = args.intent;
        console.log(JSON.stringify(intent));
        session.endDialog();
    }
]).triggerAction({
    matches: '多筆訂書'
});

bot.dialog("系列訂書",[
    function (session,args,next) {
        wlog("系列訂書");
        session.send("系列訂書");
        var intent = args.intent;
        console.log(JSON.stringify(intent));
        session.endDialog();
    }
]).triggerAction({
    matches: '系列訂書'
});

bot.dialog("None",[
    function (session,args,next) {
        wlog("無法理解請重新輸入");
        session.send("無法理解請重新輸入");
        session.endDialog();
    }
]).triggerAction({
    matches: 'None'
});

/*===============================================================
Function ↓↓↓
 ================================================================*/

//訂購
function processSubmitAction(session, value) {
    console.log(value);
    switch (value.type){
        case 'bookOrder':
            // Set the headers
            var headers = {
                'Content-Type':'application/x-www-form-urlencoded'
            }
            // Configure the request
            var options = {
                url: hlsqlapi_host+hlsqlapi_path_saveOrder,
                method: 'POST',
                headers: headers,
                form: {'CUSTOM_ID': value["CUSTOM_ID"], 'BOOK_ID':value['bookChoices'],'ORDER_NUM': value["ORDER_NUM"],'ORDER_TIME': value["ORDER_TIME"],'MEMO': value["MEMO"]}
            }
            // Start the request
            request(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    // Print out the response body
                    console.log(body);
                    session.send("訂單成立。訂單編號:"+body);
                }
            })
            session.send(value["bookChoices"]);
            break;
        default:
            session.send("訂購結束");
    }
}

//儲存對話
function saveTalk(session) {
    var nowMsg = session.message.text;
    var nowDate = new Date();
    var dateStr = dateformat(nowDate,"yyyy-mm-dd HH:MM:ss.l");
    var headers = {
        'Content-Type':'application/x-www-form-urlencoded'
    }
    // Configure the request
    //wlog("URL: "+hlsqlapi_host+hlsqlapi_path_savetalk);
    var options = {
        url: hlsqlapi_host+hlsqlapi_path_savetalk,
        method: 'POST',
        headers: headers,
        form: {'TALKTIME':dateStr, 'RESPONSER':'測試用戶', 'MSGCONTENT':nowMsg }
    }
    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            //wlog("Success: "+body);
            console.log(body);
        }
    })
}

//分析LUIS Intent JSON，結合搜尋用的書名字串
function analyzeBookName(intent) {
    var bookNameEntity = "";
    var SED  = require("./simpleEntityDefine.json");//Simple Entity  定義
    var typeDefine = SED["nameWords"];//book name type Simple Entity  定義
    if(intent.compositeEntities.length!=0){
        //複合詞分析
        var compEnts = intent.compositeEntities;
        //書名複合詞
        for(i=0; i<compEnts.length; i++){
            if(compEnts[i]["parentType"]=="書名"){
                //session.send(compEntys[i]["value"]);
                bookNameEntity = compEnts[i]["value"]
            }
        }
    }
    if(intent.entities.length!=0 && bookNameEntity=="") {
        //如無書名複合詞改獨立單辭收集送分析
        var simpleEnts = intent.entities;
        for(i=0; i<simpleEnts.length; i++){
            for(j=0; j<typeDefine.length; j++){
                if(simpleEnts[i]["type"]==typeDefine[j]){
                    //console.log("type:"+ents[i]["type"]);
                    bookNameEntity += simpleEnts[i]["entity"].toString();
                }
            }
        }
    }
    return bookNameEntity;
}

//分析訂購數量
function analyzeOrderNum(intent) {
    var OrderNumEntity = "";
    var SED  = require("./simpleEntityDefine.json");//Simple Entity  定義
    var typeDefine = SED["numWords"];//type Simple Entity  定義
    if(intent.compositeEntities.length!=0){
        //複合詞分析
        var compEnts = intent.compositeEntities;
        //數量複合詞
        for(i=0; i<compEnts.length; i++){
            if(compEnts[i]["parentType"]=="訂購數量"){
                OrderNumEntity = compEnts[i]["value"]
            }
        }
    }
    if(intent.entities.length!=0 && OrderNumEntity=="") {
        //如無複合詞改獨立單辭收集
        var simpleEnts = intent.entities;
        for(i=0; i<simpleEnts.length; i++){
            for(j=0; j<typeDefine.length; j++){
                if(simpleEnts[i]["type"]==typeDefine[j]){
                    //console.log("type:"+ents[i]["type"]);
                    OrderNumEntity += simpleEnts[i]["entity"].toString();
                }
            }
        }
    }
    return OrderNumEntity;
}

//分析外註
function analyzeOrderMemo(intent) {
    var MemoEntity = "";
    var SED  = require("./simpleEntityDefine.json");//Simple Entity  定義
    var typeDefine = SED["memoWords"];//type Simple Entity  定義
    if(intent.compositeEntities.length!=0){
        //複合詞分析
        var compEnts = intent.compositeEntities;
        for(i=0; i<compEnts.length; i++){
            if(compEnts[i]["parentType"]=="外註"){
                MemoEntity = compEnts[i]["value"]
            }
        }
    }
    if(intent.entities.length!=0 && MemoEntity=="") {
        //如無複合詞改獨立單辭收集
        var simpleEnts = intent.entities;
        for(i=0; i<simpleEnts.length; i++){
            for(j=0; j<typeDefine.length; j++){
                if(simpleEnts[i]["type"]==typeDefine[j]){
                    MemoEntity += simpleEnts[i]["entity"].toString();
                }
            }
        }
    }
    return MemoEntity;
}

//分析客戶名稱
function analyzeOrderCustom(intent) {
    var CustomEntity = "";
    var SED  = require("./simpleEntityDefine.json");//Simple Entity  定義
    var typeDefine = SED["customWords"];//order number type Simple Entity  定義
    if(intent.compositeEntities.length!=0){
        //複合詞分析
        var compEnts = intent.compositeEntities;
        for(i=0; i<compEnts.length; i++){
            if(compEnts[i]["parentType"]=="收貨方"){
                CustomEntity = compEnts[i]["value"]
            }
        }
    }
    if(intent.entities.length != 0 && CustomEntity=="") {
        //如無複合詞改獨立單辭收集
        var simpleEnts = intent.entities;
        for(i=0; i<simpleEnts.length; i++){
            for(j=0; j<typeDefine.length; j++){
                if(simpleEnts[i]["type"]==typeDefine[j]){
                    CustomEntity += simpleEnts[i]["entity"].toString();
                }
            }
        }
    }
    return CustomEntity;
}

//api 連線測試
function testapi() {
    var headers = {
        'Content-Type':'application/x-www-form-urlencoded'
    }
    // Configure the request
    var options = {
        host: "hlsqlapi.azurewebsites.net",
        path:"/test",
        method: 'GET'
        //headers: headers
    }
    wlog("testapi");
    // Start the request
   http.request(options, function (res) {
        wlog("TEST CONNECT STATUS："+res.statusCode);
   }).end();
}

//api 連線測試 - 使用 node-rest-client
function testapi2() {
    var nrclient = new nrClient();
    // direct way
    nrclient.get("http://hlsqlapi.azurewebsites.net/test", function (data, response) {
        // parsed response body as js object
        //console.log("data: " + data);
        //console.log("response: " + response);
        wlog("TEST CONNECT OK!");
        // raw response
    });
}

//write text log
function wlog(message) {
    fs.appendFile("log.txt", new Date().toString()+"-->"+message+"\r\n", function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
}